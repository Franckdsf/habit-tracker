import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
Vue.use(Vuex)
Vue.use(VueResource)

// let habitudes = [{
//   type: "Sante",
//   libelle: "Boire de leau tous les matins",
//   pourcent: 30,
//   color: "#ff894f"
// },
// {
//   type: "Culture",
//   libelle: "Boire de leau tous les matins",
//   pourcent: 20,
//   color: "#6645b6"
// },
// {
//   type: "Sport",
//   libelle: "Boire de leau tous les matins",
//   pourcent: 20,
//   color: "#ffd349"
// },
// {
//   type: "Ménage",
//   libelle: "Boire de leau tous les matins",
//   pourcent: 100,
//   color: "#914ba0"
// },
// {
//   type: "Etude",
//   libelle: "Boire de leau tous les matins",
//   pourcent: 100,
//   color: "#41a338"
// }
// ]
function initHabitudes(){
  if (localStorage.habitudes) {
    return JSON.parse(localStorage.habitudes)
  } else {
    return []
  }
}

const state = {
  habitudes: initHabitudes()
}
const getters = {
  GET_HABITUDES: state => {
    return state.habitudes
  }
}
const mutations = {
  addHabitude (state, habitude) {
    state.habitudes.push(habitude)
    localStorage.habitudes = JSON.stringify(state.habitudes)
    console.log(localStorage.habitudes)
  }
}
const actions = {
  addHabitude ({commit}, habitude) {
    return new Promise((resolve) => {
      commit("addHabitude",habitude)
      resolve(true)
    })
  }
}
const store = {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
  }
  
  export default store